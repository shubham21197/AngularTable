import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {JsonService} from './json.service'
import { AppComponent } from './app.component';
import { ChildComponent } from './child/child.component';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipe } from './filter.pipe';

@NgModule({
	declarations: [
	AppComponent,
	ChildComponent,
	FilterPipe
	],
	imports: [
	BrowserModule,
	FormsModule,
	HttpClientModule
	],
	providers: [JsonService],
	bootstrap: [AppComponent]
})
export class AppModule { }
