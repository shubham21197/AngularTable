import { Component } from '@angular/core';
import {JsonService} from './json.service'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	jsonArr = [];

	constructor(private myservice: JsonService) {

	}

	ngOnInit() {
      this.myservice.getJSON().subscribe(data => {
            this.jsonArr=data;
        });
  	}


  	onClickSubmit(data) {
      //alert("First Name : " + data.fname);
      this.jsonArr.push(data);
  	}

  	clearTable() {
  	this.jsonArr = [];
  	}

}
