import { Component, OnInit, Input } from '@angular/core';
import {JsonService} from './../json.service'

@Component({
	selector: 'app-child',
	templateUrl: './child.component.html',
	styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

	constructor(private myservice: JsonService) { }

	ngOnInit() {
	}

	@Input() Jarr;

	sortTable(ind: number) {
		this.Jarr=this.myservice.sortJSON(this.Jarr,ind);
		this.myservice.adone[ind] = !this.myservice.adone[ind];
	}

	sortlname() {
		this.Jarr=this.myservice.sortJSON(this.Jarr,1);
		this.myservice.adone[1] = !this.myservice.adone[1];
	}

	sortaddr() {
		this.Jarr=this.myservice.sortJSON(this.Jarr,2);
		this.myservice.adone[2] = !this.myservice.adone[2];
	}

	





  //console.log(this.Jarr);

}
