import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class JsonService {

	adone=[false,true,true];
	anames=["fname","lname","addr"];

	constructor(private http: HttpClient) {
		this.getJSON().subscribe(data => {
			console.log(data)
		});
	}


	public getJSON(): Observable<any> {
		return this.http.get("./assets/file.json")
	}

	public sortJSON(Jarr: any, sor: number): Observable<any> {
		
		Jarr.sort(predicateBy(this.anames[sor],this.adone[sor]));
		console.log(this.adone[0]);
		return Jarr;
	}

	function predicateBy(prop,done){
		if(done){
			return function(a,b){
				if( a[prop] > b[prop]){
					return 1;
				}else if( a[prop] < b[prop] ){
					return -1;
				}
			}
		}
		else{
			return function(a,b){
				if( a[prop] > b[prop]){
					return -1;
				}else if( a[prop] < b[prop] ){
					return 1;
				}}
			}

		return 0;
	}
}

